const CLIENT_CODES = {
    SEND_PRIVATE_MESSEGE: 102,
    SIGNUP: 103,
    LOGIN: 104
  }

  module.exports = CLIENT_CODES;