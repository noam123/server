const SERVER_CODES = require('../consts/server-codes');
const manageDB = require('../db/database-management');
const updateChatsList = require('./update-chats-list');

module.exports = {
  sendPrivateMessage: function (msg, connectedUsers) {
    body = { type: SERVER_CODES.GET_PRIVATE_MESSAGE, sourceID: msg.sourceID, message: msg.message };

    // Sends the message to the destination user
    dstClientWS = connectedUsers[connectedUsers.findIndex(user => user.ID == msg.destinationID)].clientWS;
    dstClientWS.send(JSON.stringify(body));
  },

  signup: function (username, password, ws) {
    manageDB.addNewUser(username, password);
    var body = { type: SERVER_CODES.SIGNED_UP_SUCCESSFULY };
    ws.send(JSON.stringify(body));
  },

  login: function (username, password, connectedUsers, ws, wss) {
    manageDB.doesUserExist(username, password).then(user => {
      if (user != null) {
        console.log(user);
        userIndex = connectedUsers.findIndex(connUser => connUser.clientWS == ws);
        if (userIndex != -1) {
          connectedUsers[userIndex].ID = user.id;
          connectedUsers[userIndex].name = user.username;

          var body = { type: SERVER_CODES.LOGGED_IN_SUCCESSFULY, newID: user.id, username: user.username };
          ws.send(JSON.stringify(body));

          updateChatsList.sendRes(wss, connectedUsers);
        }
      }
      else {
        ws.send(JSON.stringify({ type: SERVER_CODES.FAILED }));
      }
    })
  }
}