const db = require("./config/config");

module.exports = {
  addNewUser: async function (username, password) {
    var usersDB = db.collection("users");
    var userID = (await usersDB.get()).size;
    var newUser = usersDB.doc(userID.toString());
  
    await newUser.set({
      username: username,
      password: password
    });
  },

  doesUserExist: async function (username, password) {
    var usersDB = db.collection("users");
    var snapshot = await usersDB.where('username', '==', username).
      where('password', '==', password).get();
    var user = snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }));
    if (user.length != 0) {
      return user[0];
    }
    return null;
  }

}