const fs = require("firebase-admin");

const serviceAccount = require('./messenger-plus-ni-firebase-adminsdk-uemyq-0611d13a42.json');

fs.initializeApp({
 credential: fs.credential.cert(serviceAccount)
});
const db = fs.firestore();
module.exports = db;