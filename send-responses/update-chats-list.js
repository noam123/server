const SERVER_CODES = require('../consts/server-codes')
const webSocket = require('ws');

exports.sendRes = function(wss, connectedUsers) {

  wss.clients.forEach(client => {

    usersArr = [];
    connectedUsers.forEach(user => {
      if (user.name.length > 0 && client != user.clientWS)
        usersArr.push({ ID: user.ID, name: user.name });
    });

    if (client.readyState == webSocket.OPEN) {
      body = { type: SERVER_CODES.UPDATE_CHATS_LIST, connectedUsers: usersArr };
      client.send(JSON.stringify(body));
    }
  })
}
