const express = require('express');
const http = require('http');
const webSocket = require('ws');

const CLIENT_CODES = require('./consts/client-codes');
const handleReqs = require('./send-responses/handle-client-reqs');
const updateChatsList = require('./send-responses/update-chats-list');

const port = 3000;
const server = http.createServer(express);
const wss = new webSocket.Server({ server });

var connectedUsers = [];

wss.on('connection', ws => {
  var user = { ID: -1, name: "", clientWS: ws };
  connectedUsers.push(user);
  updateChatsList.sendRes(wss, connectedUsers);

  ws.on('message', clinetMsg => {
    msgInJSON = JSON.parse(clinetMsg.toString('utf8')); // converts message from 'buffer' to 'JSON'
    handleRequest(msgInJSON, ws, wss);
  })

  ws.on('close', () => {
    userToDelete = connectedUsers.findIndex(user => user.clientWS == ws);
    connectedUsers.splice(userToDelete, 1);
    ws.close();

    updateChatsList.sendRes(wss, connectedUsers);
  })

  ws.on('error', (error) => console.log(error))
})

server.listen(port, () => {
  console.log(`Server is listening on port ${port}!`)
})



function handleRequest(msg, ws, wss) {
  console.log(msg);
  switch (msg.type) {
    case CLIENT_CODES.SEND_PRIVATE_MESSEGE:
      handleReqs.sendPrivateMessage(msg, connectedUsers);
      break;
    case CLIENT_CODES.SIGNUP:
      handleReqs.signup(msg.username, msg.password, ws);
      break;
    case CLIENT_CODES.LOGIN:
      handleReqs.login(msg.username, msg.password, connectedUsers, ws, wss);
      break;
  }
}

